from flask import Flask,jsonify,request,json
from flask_cors import CORS, cross_origin
from flask_pymongo import PyMongo
from bson import ObjectId


app = Flask(__name__)
CORS(app, support_credentials=True)

mongo = PyMongo(app)



@app.route("/testt")
def addUser():
	user = mongo.db.test
	user.insert({'name': 'test'})

@app.route('/add',methods=['POST'])
@cross_origin(supports_credentials=True)
def add_webinar():
   webinar = mongo.db.events	
   mentor_array=[]
   sub_heading_array=[]	
   event_type=request.json['event_type']
   event_category=request.json['event_category']
   product_category=request.json['product_category']
   title=request.json['title']
   topic=request.json['topic']
   description=request.json['description']
   from_date=request.json['from_date']
   from_time=request.json['from_time']
   to_date=request.json['to_date']
   to_time=request.json['to_time']
   city=request.json['city']
   address=request.json['address']
   google_link=request.json['google_link']
   about_the_event=request.json['about_the_event']
   url=request.json['url']
   event_cost=request.json['event_cost']
   timezone=request.json['timezone']
   status=request.json['status']
   mentor_array=request.json['mentor_array']
   sub_heading_array=request.json['sub_heading_array']
            
   result1=1
   result2=1
   length=len(mentor_array)
   if event_type=="" or title=="" or topic=="" or about_the_event=="" or description=="" or from_date=="" or from_time=="" or to_date=="" or to_time=="" or  url=="" or timezone=="" or event_category=="" or product_category=="":
	  result1=0

   for val in mentor_array:
   	if val['mentor_name']=="" or val['mentor_profile']=="" or val['mentor_designation']=="" or val['mentor_linkedin']=="" or val['mentor_company']=="" or val['mentor_image']=="" :
   		result2=0;
   		
   result=result1 and result2 and length

   if result==0:
      msg="invalid"
   else:
   	  msg="valid"
   	  insert_result=webinar.insert({'event_type':event_type,'event_category':event_category,'product_category':product_category,'title':title,'topic':topic,'description':description,'from_date':from_date,'from_time':from_time,'to_date':to_date,'to_time':to_time,'city':city,'address':address,'google_link':google_link,'about_the_event':about_the_event,'event_cost':event_cost,'timezone':timezone,'url':url,'status':status,'sub_heading_array':sub_heading_array,'mentor_array':mentor_array })  		
      
 
   return jsonify({"message":msg})

@app.route('/get',methods=['GET'])
def get_all_webinars():
   webinar = mongo.db.events
   output = []
   for q in webinar.find():
   	  output.append({'_id':str(q['_id']),'event_type':q['event_type'],'url':q['url'],'status':q['status'],'title':q['title'],'topic':q['topic'],'description':q['description'],'event_category':q['event_category'],'product_category':q['product_category'],'from_date':q['from_date'],'from_time':q['from_time'],'to_date':q['to_date'],'to_time':q['to_time'],'city':q['city'],'address':q['address'],'google_link':q['google_link'],'about_the_event':q['about_the_event'],'event_cost':q['event_cost'],'timezone':q['timezone'],'sub_heading_array':q['sub_heading_array'],'mentor_array':q['mentor_array'] })

   return jsonify({"data":output})

@app.route('/get/<id>',methods=['GET'])
def get_webinar_by_id(id):
   webinar = mongo.db.events
   q=webinar.find_one({'_id':ObjectId(id)})
   output1={'event_type':q['event_type'],'url':q['url'],'status':q['status'],'title':q['title'],'topic':q['topic'],'description':q['description'],'event_category':q['event_category'],'product_category':q['product_category'],'from_date':q['from_date'],'from_time':q['from_time'],'to_date':q['to_date'],'to_time':q['to_time'],'city':q['city'],'address':q['address'],'google_link':q['google_link'],'about_the_event':q['about_the_event'],'event_cost':q['event_cost'],'timezone':q['timezone'],'sub_heading_array':q['sub_heading_array'],'mentor_array':q['mentor_array'] }
   # output={'title':q['title'],'topic':q['topic'],'description':q['description'],'seminar_date':q['seminar_date'],'start_time':q['start_time'],'course':q['course'],'category':q['category'],'duration':q['duration'],'about_the_seminar':q['about_the_seminar'],'requirement':q['requirement'],'mentor':q['mentor'] }

   return jsonify({"data":output1})    

@app.route('/update/<id>',methods=['PUT'])
def updatet_webinar(id):
   webinar = mongo.db.webinars
   mentors=[]	
   title=request.json['title']
   topic=request.json['topic']
   description=request.json['description']
   seminar_date=request.json['seminar_date']
   start_time=request.json['start_time']
   duration=request.json['duration']
   course=request.json['course']
   category=request.json['category']
   about_the_seminar=request.json['about_the_seminar']
   requirements=request.json['requirement']
   mentors=request.json['mentor']
   result1=1
   result2=1
   length=len(mentors)
   if title=="" or topic=="" or description=="" or seminar_date=="" or duration=="" or  course=="" or category=="" or about_the_seminar=="" or requirements=="":
	  result1=0	

   for val in mentors:
   	if val['mentor_name']=="" or val['mentor_profile']=="" or val['mentor_designation']=="" or val['mentor_linkedin']=="" or val['mentor_company']=="" or val['mentor_image']=="" :
   		result2=0;
   		
   result=result1 & result2
   if result==0:
      msg="invalid data"
   else:
   	  msg="data updated in database"
   	  q=webinar.update({'_id':ObjectId(id)},{'$set':{'title':title,'topic':topic,'description':description,'seminar_date':seminar_date,'start_time':start_time,'course':course,'category':category,'duration':duration,'about_the_seminar':about_the_seminar,'requirement':requirements,'mentor':mentors}},multi=True)
   
   return jsonify({"msg":msg})       

if __name__ == '__main__':
   app.run(debug=True)

# 'description':description,'seminar_date':seminar_date,'start_time':start_time,'course':course,'category':category,'duration':duration,'about_the_seminar':about_the_seminar,'requirement':requirements,'mentor':mentors